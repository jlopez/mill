SUBMISSIONDIR="$ROOTDIR/submissions"
TESTSDIRS=("$ROOTDIR/tests/fract" "$ROOTDIR/tests/from_bin" "$ROOTDIR/tests/str" "$ROOTDIR/tests/transp")
TESTCMDS=(./fract.x ./from_bin.x ./str.x ./transp.x)
TESTARGS=("" "" "" "")
REFNAME="ref"

NEGATIVEGRADES="false"
TOTEST="out code"
DEFAULTCODE='0'
DEFAULTIN=
DEFAULTOUT=
DEFAULTERR=

function prepare
{
  make > /dev/null
}

function cleanup
{
  make clean > /dev/null
}
