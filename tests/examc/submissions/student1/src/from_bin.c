#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned int bin_to_dec(const char *bin);

unsigned int bin_to_dec(const char *bin) {
  const int len = strlen(bin);
  unsigned int dec = 0;
  int multiplier = 1;
  for (int i = len - 1; i >= 0; i--) {
    if (bin[i] != '0' && bin[i] != '1')
      exit(2);
    dec += (bin[i] - '0') * multiplier;
    multiplier *= 2;
  }
  return dec;
}

int main(int argc, const char *argv[]) {
  if (argc != 2) {
    printf("<< Error : expecting only one argument. >>\n");
    return 0;
  }
  printf("%u\n", bin_to_dec(argv[1]));
  return 0;
}
