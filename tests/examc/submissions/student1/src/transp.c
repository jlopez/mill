#include <stdio.h>
#include <stdlib.h>
#define DIM 5

int *get_matrix(void);
void transpose(int *mat);
void print_matrix(int *mat);

int *get_matrix(void) {
  int *mat = malloc((DIM * DIM) * sizeof (int));
  int curr;
  int err;

  for (int i = 0; i < DIM; i++)
    for (int j = 0; j < DIM; j++) {
      err = scanf("%d", &curr);
      if (err == EOF)
        exit(1);
      mat[i * DIM + j] = curr;
    }
  return mat;
}

void transpose(int *mat) {
  int swap;

  for (int i = 0; i < DIM; i++)
    for (int j = i + 1; j < DIM; j++) {
      swap = mat[i * DIM + j];
      mat[i * DIM + j] = mat[j * DIM + i];
      mat[j * DIM + i] = swap;
    }
}

void print_matrix(int *mat) {
  for (int i = 0; i < DIM; i++) {
    for (int j = 0; j < DIM; j++)
      printf("%d ", mat[i * DIM + j]);
    printf("\n");
  }
}

int main(void) {
  int *mat = get_matrix();
  transpose(mat);
  print_matrix(mat);
  return 0;
}
