#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, const char *argv[]) {
  if (argc == 1)
    return 0;
  int len = 0;

  for (int i = 1; i < argc; i++)
    len += strlen(argv[i]);
  char *res = malloc((len + argc) * sizeof (char));
  res[0] = '\0';
  strcat(res, argv[1]);
  for (int i = 2; i < argc; i++) {
    strcat(res, " ");
    strcat(res, argv[i]);
  }
  printf("%s\n", res);
  free(res);
  return 0;
}
