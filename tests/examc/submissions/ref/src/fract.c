#include <stdio.h>
#include "fract.h"

unsigned int pgcd(const unsigned int a, const unsigned int b);
void norm(struct fraction_t *frac);

unsigned int pgcd(const unsigned int a, const unsigned int b) {
  if (a == b)
    return a;
  if (a < b)
    return pgcd(a, b - a);
  return pgcd(a - b, b);
}

void norm(struct fraction_t *frac) {
  const unsigned int normer = pgcd(frac->num, frac->denom);
  frac->num /= normer;
  frac->denom /= normer;
}

int main(void) {
  char sign;
  int num;
  int denom;
  int err = scanf("%c", &sign);

  if (err != 1 || (sign != '+' && sign != '-'))
    return 1;

  err = scanf("%d", &num);
  err = scanf("%d", &denom);

  struct fraction_t frac = {sign, num, denom};
  norm(&frac);
  if (frac.sign == '-')
    printf("%c%d/%d\n", frac.sign, frac.num, frac.denom);
  else
    printf("%d/%d\n", frac.num, frac.denom);
  return 0;
}
