SUBMISSIONDIR="$ROOTDIR/submissions"
TESTSDIRS=("$ROOTDIR/tests")
GETCMD="tar -xf"
TESTCMDS=(./palindrome)
TESTARGS=("")
REFNAME="ref.tar.gz"

NEGATIVEGRADES="false"
TOTEST="out"
DEFAULTCODE='0'
DEFAULTIN=
DEFAULTOUT=
DEFAULTERR=

function prepare
{
  make > /dev/null
}
