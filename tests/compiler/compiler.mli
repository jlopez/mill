type expr =
| Var of var
| Fun of var * expr
| App of expr * expr
| UnOp of unop * expr
| BinOp of binop * expr * expr
| Integer of int
| String of string
| Boolean of bool
and var = string
and unop =
| Not
and binop =
| Plus
| Minus
| Star
| Slash
| Or
| And
| Equal
| LessThan
| LessOrEqual

type value =
| VFun of var * expr
| VInteger of int
| VString of string
| VBoolean of bool

exception Error of string

val parse: string -> expr
val eval: expr -> value
