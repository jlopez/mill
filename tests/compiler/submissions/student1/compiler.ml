type expr =
| Var of var
| Fun of var * expr
| App of expr * expr
| UnOp of unop * expr
| BinOp of binop * expr * expr
| Integer of int
| String of string
| Boolean of bool
and var = string
and unop =
| Not
and binop =
| Plus
| Minus
| Star
| Slash
| Or
| And
| Equal
| LessThan
| LessOrEqual

type value =
| VFun of var * expr
| VInteger of int
| VString of string
| VBoolean of bool

exception Error of string

module Env = Map.Make(String)

let parse s = Integer 5

let eval e =
let rec aux vars = function
| Var v ->
  begin
    try Env.find v vars
    with Not_found -> raise (Error("Unbound variable " ^ v))
  end
| Fun(v, e) -> VFun(v, e)
| App(e1, e2) ->
  begin
    let f = aux vars e1 in
    match f with
    | VFun(v, e) -> aux (Env.add v (aux vars e2) vars) e
    | _ -> raise (Error("Function expected at left-hand side of application."))
  end
| UnOp(op, e) ->
  begin
    let v = aux vars e in
    match op, v with
    | Not, VBoolean b -> VBoolean(not b)
    | Not, _ -> raise (Error("Boolean expected after not operator."))
  end
| BinOp(op, e1, e2) ->
  begin
    let v1 = aux vars e1 in
    let v2 = aux vars e2 in
    match op, v1, v2 with
    | Plus, VInteger i1, VInteger i2 -> VInteger(i1 + i2)
    | Plus, _, _ -> raise (Error("Integers expected for plus operator."))
    | Minus, VInteger i1, VInteger i2 -> VInteger(i1 - i2)
    | Minus, _, _ -> raise (Error("Integers expected for minus operator."))
    | Star, VInteger i1, VInteger i2 -> VInteger(i1 * i2)
    | Star, _, _ -> raise (Error("Integers expected for star operator."))
    | Slash, VInteger i1, VInteger i2 -> VInteger(i1 / i2)
    | Slash, _, _ -> raise (Error("Integers expected for slash operator."))
    | Or, VBoolean b1, VBoolean b2 -> VBoolean(b1 || b2)
    | Or, _, _ -> raise (Error("Booleans expected for or operator."))
    | And, VBoolean b1, VBoolean b2 -> VBoolean(b1 && b2)
    | And, _, _ -> raise (Error("Booleans expected for and operator."))
    | Equal, VFun _, VFun _ -> raise (Error("Equality for functions unsupported."))
    | Equal, VInteger v1, VInteger v2 -> VBoolean(v1 = v2)
    | Equal, VString v1, VString v2 -> VBoolean(v1 = v2)
    | Equal, VBoolean v1, VBoolean v2 -> VBoolean(v1 = v2)
    | Equal, _, _ -> raise (Error("Same type expected on both side of equal operator."))
    | LessThan, VInteger i1, VInteger i2 -> VBoolean(i1 < i2)
    | LessThan, _, _ -> raise (Error("Integers expected for lt operator."))
    | LessOrEqual, VInteger i1, VInteger i2 -> VBoolean(i1 <= i2)
    | LessOrEqual, _, _ -> raise (Error("Integers expected for loe operator."))  end
| Integer i -> VInteger i
| String i -> VString i
| Boolean i -> VBoolean i
in
aux Env.empty e

let rec print_value = function
| VFun(v, e) -> Printf.printf "<fun>\n"
| VInteger i -> Printf.printf "%d\n" i
| VString s -> Printf.printf "%s\n" s
| VBoolean b -> Printf.printf "%b\n" b

let _ =
for i = 1 to Array.length Sys.argv - 1 do
  let inchan = open_in Sys.argv.(i) in
  print_value (eval (parse (input_line inchan)))
done
