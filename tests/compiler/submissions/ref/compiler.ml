type expr =
| Var of var
| Fun of var * expr
| App of expr * expr
| UnOp of unop * expr
| BinOp of binop * expr * expr
| Integer of int
| String of string
| Boolean of bool
and var = string
and unop =
| Not
and binop =
| Plus
| Minus
| Star
| Slash
| Or
| And
| Equal
| LessThan
| LessOrEqual

type value =
| VFun of var * expr
| VInteger of int
| VString of string
| VBoolean of bool

exception Error of string

module SMap = Map.Make(String)

module Parser = struct
  type token =
  | TId of string
  | TFun
  | TNot
  | TPlus
  | TMinus
  | TStar
  | TSlash
  | TOr
  | TAnd
  | TEqual
  | TLessThan
  | TLessOrEqual
  | TInteger of int
  | TString of string
  | TBoolean of bool

  let keywords =
    let l =
      [ ("fun", TFun); ("!", TNot); ("+", TPlus); ("-", TMinus); ("*", TStar);
        ("/", TSlash); ("|", TOr); ("&", TAnd); ("=", TEqual);
        ("<", TLessThan); ("<=", TLessOrEqual); ("true", TBoolean true);
        ("false", TBoolean false) ] in
    let map = ref SMap.empty in
    List.iter (fun (k, v) -> map := SMap.add k v !map) l;
    !map

  let string_of_token = function
  | TId i -> i
  | TFun -> "fun"
  | TNot -> "!"
  | TPlus -> "+"
  | TMinus -> "-"
  | TStar -> "*"
  | TSlash -> "/"
  | TOr -> "|"
  | TAnd -> "&"
  | TEqual -> "="
  | TLessThan -> "<"
  | TLessOrEqual -> "<="
  | TInteger i -> string_of_int i
  | TString s -> String.make 1 '"' ^ s ^ String.make 1 '"'
  | TBoolean b -> string_of_bool b

  let lex s =
    let tokens = ref [] in
    let len = String.length s in
    let word = ref "" in
    let consume () =
      let newtoken =
        try SMap.find !word keywords
        with Not_found ->
          try TInteger (int_of_string !word)
          with Failure _ -> TId !word
      in
      word := "";
      tokens := newtoken :: !tokens
    in
    for i = 0 to len - 1 do
      match String.get s i with
      | ' ' | '\t' | '\n' | '\r' | '\b' -> consume ()
      | c -> word := !word ^ String.make 1 c
    done;
    consume ();
    List.rev !tokens

  let parse s =
    let tokens = Array.of_list (lex s) in
    let n = Array.length tokens in
    let i = ref 0 in
    let next () = i := !i + 1 in
    let rec parse_expr () = match tokens.(!i) with
    | TId i -> Var i
    | TFun ->
      next ();
      let f = parse_fun () in
      next ();
      if !i = n then f else App (f, parse_expr ())
    | TNot -> next (); UnOp (Not, parse_expr ())
    | TPlus | TMinus | TStar | TSlash | TOr | TAnd | TEqual | TLessThan
    | TLessOrEqual ->
      let bop = match tokens.(!i) with | TPlus -> Plus | TMinus -> Minus
        | TStar -> Star | TSlash -> Slash | TOr -> Or | TAnd -> And
        | TEqual -> Equal | TLessThan -> LessThan | TLessOrEqual -> LessOrEqual
        | _ -> raise (Error "Unreachable code reached")
      in
      next ();
      let e1 = parse_expr () in
      next ();
      let e2 = parse_expr () in
      BinOp (bop, e1, e2)
    | TInteger i -> Integer i
    | TString s -> String s
    | TBoolean b -> Boolean b
    and parse_fun () =
      let var =
        match tokens.(!i) with
        | TId v -> v
        | _ -> raise (Error "Expected variable")
      in
      next ();
      Fun (var, parse_expr ())
    in
    let e = parse_expr () in
    next ();
    if !i != n then raise (Error "Expected EOF");
    e
end

let parse s = Parser.parse s

let eval e =
let rec aux vars = function
| Var v ->
  begin
    try SMap.find v vars
    with Not_found -> raise (Error("Unbound variable " ^ v))
  end
| Fun(v, e) -> VFun(v, e)
| App(e1, e2) ->
  begin
    let f = aux vars e1 in
    match f with
    | VFun(v, e) -> aux (SMap.add v (aux vars e2) vars) e
    | _ -> raise (Error("Function expected at left-hand side of application."))
  end
| UnOp(op, e) ->
  begin
    let v = aux vars e in
    match op, v with
    | Not, VBoolean b -> VBoolean(not b)
    | Not, _ -> raise (Error("Boolean expected after not operator."))
  end
| BinOp(op, e1, e2) ->
  begin
    let v1 = aux vars e1 in
    let v2 = aux vars e2 in
    match op, v1, v2 with
    | Plus, VInteger i1, VInteger i2 -> VInteger(i1 + i2)
    | Plus, _, _ -> raise (Error("Integers expected for plus operator."))
    | Minus, VInteger i1, VInteger i2 -> VInteger(i1 - i2)
    | Minus, _, _ -> raise (Error("Integers expected for minus operator."))
    | Star, VInteger i1, VInteger i2 -> VInteger(i1 * i2)
    | Star, _, _ -> raise (Error("Integers expected for star operator."))
    | Slash, VInteger i1, VInteger i2 -> VInteger(i1 / i2)
    | Slash, _, _ -> raise (Error("Integers expected for slash operator."))
    | Or, VBoolean b1, VBoolean b2 -> VBoolean(b1 || b2)
    | Or, _, _ -> raise (Error("Booleans expected for or operator."))
    | And, VBoolean b1, VBoolean b2 -> VBoolean(b1 && b2)
    | And, _, _ -> raise (Error("Booleans expected for and operator."))
    | Equal, VFun _, VFun _ -> raise (Error("Equality for functions unsupported."))
    | Equal, VInteger v1, VInteger v2 -> VBoolean(v1 = v2)
    | Equal, VString v1, VString v2 -> VBoolean(v1 = v2)
    | Equal, VBoolean v1, VBoolean v2 -> VBoolean(v1 = v2)
    | Equal, _, _ -> raise (Error("Same type expected on both side of equal operator."))
    | LessThan, VInteger i1, VInteger i2 -> VBoolean(i1 < i2)
    | LessThan, _, _ -> raise (Error("Integers expected for lt operator."))
    | LessOrEqual, VInteger i1, VInteger i2 -> VBoolean(i1 <= i2)
    | LessOrEqual, _, _ -> raise (Error("Integers expected for loe operator."))
  end
| Integer i -> VInteger i
| String s -> VString s
| Boolean b -> VBoolean b
in
aux SMap.empty e

let rec print_value = function
| VFun(v, e) -> Printf.printf "<fun>\n"
| VInteger i -> Printf.printf "%d\n" i
| VString s -> Printf.printf "%s\n" s
| VBoolean b -> Printf.printf "%b\n" b

let _ =
for i = 1 to Array.length Sys.argv - 1 do
  let inchan = open_in Sys.argv.(i) in
  print_value (eval (parse (input_line inchan)))
done
