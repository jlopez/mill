#!/usr/bin/env bash

SUBMISSIONDIR="$ROOTDIR/submissions"
TESTSDIRS=("$ROOTDIR/tests")
TESTCMDS=(./a.out)
TESTARGS=("")
REFNAME="ref"

NEGATIVEGRADES="false"
TOTEST="out"
DEFAULTCODE='0'
DEFAULTIN=
DEFAULTOUT=
DEFAULTERR=

function prepare
{
  cp $ROOTDIR/compiler.mli .
  ocamlopt -c compiler.mli
  ocamlopt compiler.ml
  cp ${TESTSDIRS[$i]}/*.file .
}

function cleanup
{
  rm -f a.out compiler.mli compiler.cmi compiler.cmx compiler.o *.file
}
