#!/usr/bin/env bash

# Exists on error or use of unused variable
set -euo pipefail

# Empty match -> empty array
shopt -s nullglob

## User variables

SUBMISSIONDIR="" # Where the submissions are located
ROOTSUBMISSION="" # The root of a submission, empty will let the script find a
                  # suitable candidate
TESTSDIRS=() # The directories where tests are
GETCMD="" # Command to obtain submission
TESTCMDS=() # Commands to launch to get outputs from a submission for each
             # test directory
TESTARGS=() # Arguments of the TESTCMDS
REFNAME="" # The name of the reference submission

NEGATIVEGRADES="false" # Are negative grades allowed?
TIMEOUT=2
TOTEST="code out err"
DEFAULTCODE='0'
DEFAULTIN=
DEFAULTOUT=
DEFAULTERR=

# Compile stuff, run servers, scripts, ...
# If returns something other than 0, runtests will not be called
function prepare
{
  return 0
}

# Cleanup after testing a submission
function cleanup
{
  return 0
}

# Prepare for a test
function before_each_test
{
  return 0
}

# Cleanup after a test
function after_each_test
{
  return 0
}

## Internal variables

ROOTDIR='.'
TAR='tar'
RM='rm -f'
CHECKREF='true'

## Tests a submission
function runtests
{
  # For every test
  for tin in ${TESTSDIRS[$i]}/*.in; do
    TESTNAME="$(printf $tin | sed 's/\.in$//')"
    GENERATED="$TESTNAME.submission.out $TESTNAME.submission.err $TESTNAME.submission.code"
    # If no ref, create default one
    test -f $TESTNAME.code || GENERATED=$(printf "$DEFAULTCODE" > $TESTNAME.code && printf "$GENERATED $TESTNAME.code")
    test -f $TESTNAME.in || GENERATED=$(printf "$DEFAULTIN" > $TESTNAME.in && printf "$GENERATED $TESTNAME.in")
    test -f $TESTNAME.out || GENERATED=$(printf "$DEFAULTOUT" > $TESTNAME.out && printf "$GENERATED $TESTNAME.out")
    test -f $TESTNAME.err || GENERATED=$(printf "$DEFAULTERR" > $TESTNAME.err && printf "$GENERATED $TESTNAME.err")
    before_each_test
    # If additional args, add them
    THISTESTARGS="${TESTARGS[$i]} $(test -f $TESTNAME.args && cat $TESTNAME.args)"

    # Test the submission on TESTNAME
    printf "Testing %s:\n" $(basename $TESTNAME) >> $SUBMISSIONLOG
    ERR=0
    { cat $tin | timeout $TIMEOUT ${TESTCMDS[$i]} $THISTESTARGS; } > $TESTNAME.submission.out 2> $TESTNAME.submission.err || ERR=$?

    # If timeout happens
    if test $ERR -eq 124; then
      printf "[FAILED] Timeout of $TIMEOUT seconds.\n" >> $SUBMISSIONLOG
      SUCCESS='false'
    else
      printf $ERR > $TESTNAME.submission.code
      SUCCESS='true'
      for ext in $TOTEST; do
        ERR=0
        diff $TESTNAME.$ext $TESTNAME.submission.$ext > /dev/null 2>&1 || ERR=$?
        if test $ERR -ne 0; then
          printf "[FAILED] Expected $ext was:\n" >> $SUBMISSIONLOG
          cat $TESTNAME.$ext >> $SUBMISSIONLOG
          printf "but got:\n" >> $SUBMISSIONLOG
          cat $TESTNAME.submission.$ext >> $SUBMISSIONLOG
          SUCCESS='false'
        fi
      done
    fi

    # Cleanup
    $RM $GENERATED
    # One point given per success
    NBRTESTS=$(($NBRTESTS + 1))
    if test $SUCCESS = 'true'; then
      GRADE=$(($GRADE + 1))
      printf "[SUCCESS]\n" >> $SUBMISSIONLOG
    fi
    after_each_test
  done
  return 0
}

## Starting point
function main
{
  cd $SUBMISSIONDIR

  for SUBMISSION in *; do
    TOTALGRADE=0
    TOTALTESTS=0
    test "${SUBMISSION: -4}" = ".log" && continue
    test "$CHECKREF" = "false" && test "$SUBMISSION" = "$REFNAME" && continue
    SUBMISSIONLOG=$SUBMISSIONDIR/$SUBMISSION.log

    printf "" > $SUBMISSIONLOG

    # Getting the submission code
    if test "$GETCMD" != ""; then
      ERR=0
      $GETCMD $SUBMISSION > /dev/null 2>&1 || ERR=$?
      if test $ERR -ne 0; then
        printf "[ERROR] $SUBMISSION is invalid.\n" | tee -a $SUBMISSIONLOG
        continue
      fi
    fi

    ROOT="$ROOTSUBMISSION"
    # If ROOTSUBMISSION is empty, find it
    if test "$ROOTSUBMISSION" = ""; then
      if test -d "$SUBMISSION"; then
        ROOT="$SUBMISSION"
      else
        ROOT=$(find . -mindepth 1 -maxdepth 1 -type d | head -n 1)
      fi
    fi
    cd $ROOT

    ERR=0
    { prepare >> $SUBMISSIONLOG 2>&1; } || ERR=$?
    if test $ERR -eq 0; then
      # Testing the submission code
      for i in $(seq 0 $((${#TESTSDIRS[@]} - 1))); do
	GRADE=0
	NBRTESTS=0
	SHORTTESTDIRNAME=$(basename ${TESTSDIRS[$i]})
	printf "=== %s ===\n" $SHORTTESTDIRNAME >> $SUBMISSIONLOG
	{ runtests >> $SUBMISSIONLOG 2>&1; } || ERR=$?
	test "$NEGATIVEGRADES" != "true" -a $GRADE -lt 0 && GRADE=0
	printf "[$SUBMISSION][$SHORTTESTDIRNAME] $GRADE / $NBRTESTS\n" | tee -a $SUBMISSIONLOG
	TOTALGRADE=$(($TOTALGRADE + $GRADE))
	TOTALTESTS=$(($TOTALTESTS + $NBRTESTS))
      done
    fi
    cleanup
    cd $SUBMISSIONDIR

    # Print final grade and cleanup
    test "$GETCMD" != "" && $RM -r $ROOT
    printf "[$SUBMISSION] $TOTALGRADE / $TOTALTESTS\n" | tee -a $SUBMISSIONLOG
  done
}

function usage
{
  printf "Usage: $0 [options] <dir>\n"
  printf "where <dir> is a directory that contains a config.sh script. Set by"
  printf " default to the current directory.\n"
  printf "Valid options:\n"
  printf "  -h, --help: Prints this usage and exits.\n"
  printf "  --no-ref-check: Do not run tests on the reference submission. The
    name of the reference submission is $REFNAME. To change it, modify the
    variable REFNAME.\n"
  exit $1
}

function parse_args
{
  ISSETROOTDIR="false"
  while test $# -ne 0; do
    if test $1 = "--help" || test $1 = "-h"; then
      usage 0
    elif test $1 = "--no-ref-check"; then
      CHECKREF='false'
    elif test ${1:0:1} = "-"; then
      printf "[ERROR] Invalid option: $1.\n"
      usage 1
    elif test "$ISSETROOTDIR" = "false"; then
      ROOTDIR=$(readlink -f $1) # Get absolute path
      ISSETROOTDIR="true"
    else
      printf "[ERROR] Target directory was already set to $ROOTDIR.\n"
      usage 1
    fi
    shift
  done
  unset ISSETROOTDIR
  . $ROOTDIR/config.sh 2> /dev/null || (printf "[ERROR] config.sh not found in $ROOTDIR\n" && usage 2)
}

parse_args $@ && main
